export const TOGGLE_DEVICE = 'TOGGLE_DEVICE'

export const TOGGLE_SIDEBAR = 'TOGGLE_SIDEBAR'

export const EXPAND_MENU = 'EXPAND_MENU'

export const SWITCH_EFFECT = 'SWITCH_EFFECT'

export const STATIC_PAGE = 'STATIC_PAGE'

export const SET_BUG = 'SET_BUG'
