import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import menu from './modules/menu'
import app from './modules/app'
import bugs from './modules/bugs/'
import sidebar from './modules/sidebar/'
import auth from './modules/auth'
import layout from './modules/layout'
import modal from './modules/modal'

Vue.use(Vuex)

// let local = localStorage.getItem('expUser')
// let getJsonLocal = JSON.parse(local)
// let role
// if (getJsonLocal) {
//   role = getJsonLocal.role
// } else {
//   role = ''
// }

export const store = new Vuex.Store({
  strict: true,  // process.env.NODE_ENV !== 'production',
  actions,
  getters,
  modules: {
    menu,
    app,
    bugs,
    sidebar,
    auth,
    layout,
    modal
  },
  state: {
    // isRole: role,
    packageexport: false,
    SectionName: null

  },
  mutations: {
    setpackageexport (state, data) {
      state.packageexport = data
    },
    // setisRole (state, data) {
    //   state.isRole = data
    // },
    setSectionName (state, data) {
      state.SectionName = data
    }
  }
})

export default store
