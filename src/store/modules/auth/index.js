import {mutations} from './mutations'
import {actions} from './actions'
import {getters} from './getters'

const state = {
  user: null,
  isAuthResolved: false,
  authStatus: null
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
