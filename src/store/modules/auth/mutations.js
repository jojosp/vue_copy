export const mutations = {
  SET_USER: (state, user) => {
    console.log('in mutation user', user)
    state.user = user
  },
  SET_AUTH_STATE: (state, payload) => {
    state.isAuthResolved = payload
  }
}
