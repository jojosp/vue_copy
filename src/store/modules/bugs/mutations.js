export const mutations = {
  ADD_BUG: (state, newBug) => {
    state.bugs.push(newBug)
  },
  UPDATE_BUG_DATA: (state, data) => {
    let index = state.bugs.findIndex(i => i.bug_id === data.bug_id)
    state.bugs[index] = data
    state.bug = data
  },
  DELETE_BUG: (state, id) => {
    let index = state.bugs.findIndex(bug => bug.bug_id === id)
    state.bugs.splice(index, 1)
  },
  GET_BUGS: (state, data) => {
    state.bugs = data
  },
  GET_BUG: (state, data) => {
    state.bug = data
  },
  GET_BUG_BY_ID: (state, id) => {
    state.bug = state.bugs.find(i => i.bug_id === id)
  }
}
