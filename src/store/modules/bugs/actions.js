import axios from 'axios'
import {APIENDPOINT} from '@/config/app-config'

export const actions = {
  CREATE_BUG: async (context, payload) => {
    let {data} = await axios.post(APIENDPOINT + 'bugs/', {
      package_id: payload.package_id,
      module_id: payload.module_id,
      page_id: payload.page_id,
      bug_issuer: payload.bug_issuer,
      component_name: payload.component_name,
      bug_title: payload.bug_title,
      bug_description: payload.bug_description})
    if (!data.error) context.commit('ADD_BUG', data.bug)
    return data
  },
  GET_BUGS: async (context) => {
    let {data} = await axios.get(APIENDPOINT + 'bugs/')
    if (!data.error) context.commit('GET_BUGS', data.bugs)
    return data
  },
  GET_BUG: async (context, id) => {
    let {data} = await axios.get(APIENDPOINT + `bugs/${id}`)
    if (!data.error) context.commit('GET_BUG', data.bug)
    return data
  },
  DELETE_BUG: async (context, id) => {
    let {data} = await axios.delete(APIENDPOINT + `bugs/${id}`)
    if (!data.error) context.commit('DELETE_BUG', id)
    return data
  },
  UPDATE_BUG_DATA: async (context, payload) => {
    let {bug_id, ...newData} = payload
    let {data} = await axios.put(APIENDPOINT + `bugs/${bug_id}`, newData)
    if (!data.error) context.commit('UPDATE_BUG_DATA', data.bug)
    return data
  },
  GET_BUGS_PAGINATE_AND_COUNT: async (context, payload) => {
    let {limit, offset} = payload
    let {data} = await axios.post(APIENDPOINT + 'bugs/paginateCount', {limit, offset})
    if (!data.error) context.commit('GET_BUGS', data.bugs)
    return data
  },
  GET_BUGS_PAGINATE: async (context, payload) => {
    let {limit, offset} = payload
    let {data} = await axios.post(APIENDPOINT + 'bugs/paginate', {limit, offset})
    if (!data.error) context.commit('GET_BUGS', data.bugs)
    return data
  },
  SEARCH_PAGINATE_AND_COUNT: async (context, payload) => {
    let {data} = await axios.post(APIENDPOINT + 'bugs/searchCount', payload)
    if (!data.error) context.commit('GET_BUGS', data.bugs)
    return data
  },
  SEARCH_AND_PAGINATE: async (context, payload) => {
    let {data} = await axios.post(APIENDPOINT + 'bugs/search', payload)
    if (!data.error) context.commit('GET_BUGS', data.bugs)
    return data
  }
}
