export const getters = {
  GET_BUGS: state => {
    return state.bugs
  },
  GET_BUG: state => {
    return state.bug
  },
  GET_BUG_BY_ID: state => id => {
    return state.bugs.find(i => i.bug_id === id)
  }
}
