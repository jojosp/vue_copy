import * as types from '../../mutation-types'
import lazyLoading from './lazyLoading'

const state = {
  items: [{
    name: 'Home Page',
    path: '/',
    meta: {
      requiresAuth: true,
      adminAuth: true,
      teacherAuth: true,
      editorAuth: true,
      testerAuth: true,
      icon: 'fa-tachometer',
      link: 'dashboard/index.vue'
    },
    component: lazyLoading('hello', true)
  },
  // {
  //   name: 'Create User',
  //   path: '/create',
  //   component: lazyLoading('createuser', true),
  //   meta: {
  //     requiresAuth: true,
  //     adminAuth: true,
  //     teacherAuth: false,
  //     editorAuth: false,
  //     icon: 'fa-tachometer',
  //     link: 'dashboard/index.vue'
  //   }
  // },
  {
    name: 'User Managment',
    path: '/gridusers',
    component: lazyLoading('gridusers', true),
    meta: {
      requiresAuth: true,
      adminAuth: true,
      teacherAuth: false,
      editorAuth: false,
      testerAuth: false,
      icon: 'fa-tachometer',
      link: 'dashboard/index.vue'
    }
  },
  {
    name: 'Packages',
    path: '/managepackages',
    component: lazyLoading('gridpackages', true),
    meta: {
      requiresAuth: true,
      adminAuth: true,
      teacherAuth: true,
      editorAuth: true,
      testerAuth: true,
      icon: 'fa-tachometer',
      link: 'dashboard/index.vue'
    }
  },
  {
    name: 'Update My Profile',
    path: '/update',
    component: lazyLoading('updateform', true),
    meta: {
      requiresAuth: true,
      adminAuth: true,
      teacherAuth: true,
      editorAuth: true,
      testerAuth: false,
      icon: 'fa-tachometer',
      link: 'dashboard/index.vue'
    }
  },
  {
    name: 'My Sections',
    path: '/mysections',
    component: lazyLoading('mysections', true),
    meta: {
      requiresAuth: true,
      adminAuth: true,
      teacherAuth: true,
      editorAuth: true,
      testerAuth: false,
      icon: 'fa-tachometer',
      link: 'dashboard/index.vue'
    }
  },
  {
    name: 'Bug Reports',
    path: '/bugs',
    component: lazyLoading('bugs', true),
    meta: {
      requiresAuth: true,
      adminAuth: true,
      teacherAuth: true,
      editorAuth: true,
      testerAuth: false,
      icon: 'fa-tachometer',
      link: 'dashboard/index.vue'
    }
  }
  // {
  //   name: 'Create Module',
  //   path: '/createmodule',
  //   component: lazyLoading('createmodule', true),
  //   meta: {
  //     requiresAuth: true,
  //     adminAuth: true,
  //     teacherAuth: false,
  //     editorAuth: false,
  //     icon: 'fa-tachometer',
  //     link: 'dashboard/index.vue'
  //   }
  // },
  // {
  //   name: 'Create Page',
  //   path: '/createpage',
  //   component: lazyLoading('createpage', true),
  //   meta: {
  //     requiresAuth: true,
  //     adminAuth: true,
  //     teacherAuth: true,
  //     editorAuth: false,
  //     icon: 'fa-tachometer',
  //     link: 'dashboard/index.vue'
  //   }
  // },
  // {
  //   name: 'Builder',
  //   path: '/builder',
  //   component: lazyLoading('builder', true),
  //   meta: {
  //     requiresAuth: true,
  //     adminAuth: true,
  //     teacherAuth: true,
  //     editorAuth: true,
  //     icon: 'fa-tachometer',
  //     link: 'dashboard/index.vue'
  //   }
  // }
  ]
}

const mutations = {
  [types.EXPAND_MENU] (state, menuItem) {
    if (menuItem.index > -1) {
      console.log('Mutations State')
      if (state.items[menuItem.index] && state.items[menuItem.index].meta) {
        state.items[menuItem.index].meta.expanded = menuItem.expanded
      }
    } else if (menuItem.item && 'expanded' in menuItem.item.meta) {
      menuItem.item.meta.expanded = menuItem.expanded
    }
  }
}
export default {
  state,
  mutations
}
