import {mutations} from './mutations'
import {actions} from './actions'
import {getters} from './getters'

const state = {
  padding: null
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
