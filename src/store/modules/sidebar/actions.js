export const actions = {
  SHOW_SIDEBAR: (context) => {
    context.commit('SHOW_SIDEBAR')
  },
  HIDE_SIDEBAR: (context) => {
    context.commit('HIDE_SIDEBAR')
  },
  TOGGLE_SIDEBAR: (context) => {
    context.commit('TOGGLE_SIDEBAR')
  }
}
