import TapTap from './TapTap'
import TapTapChoice from './TapTapChoice'
import TapTapText from './TapTapText'
import TapTapInput from './TapTapInput'


// export default TapTap TapTapChoice TapTapText TapTapTap;
export {
        TapTap,
        TapTapChoice,
        TapTapText,
        TapTapInput
    }
