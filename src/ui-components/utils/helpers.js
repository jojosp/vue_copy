/* eslint-disable */
/**
 * Get value of an object property/path even if it's nested
 */
export function getValueByPath (obj, path) {
  const value = path.split('.').reduce((o, i) => o[i], obj)
  return value
}

/**
 * Escape regex characters
 * http://stackoverflow.com/a/6969486
 */
export function escapeRegExpChars (value) {
  return value.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&')
}
/*
 *
 */
export function removeElement (el) {
    if (typeof el.remove !== 'undefined') {
        el.remove()
    } else {
        el.parentNode.removeChild(el)
    }
}
