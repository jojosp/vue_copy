const config = {
  defaultIconPack: 'fa',
  defaultInputAutocomplete: 'on',
  defaultModalScroll: null
}

export default config

export const setOptions = options => { this.config = options }
