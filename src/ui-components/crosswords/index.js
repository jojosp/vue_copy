import CrossWords from './CrossWords.vue'
import CrossWordsInput from './CrossWordsInput.vue'

export {
    CrossWords,
    CrossWordsInput
}
