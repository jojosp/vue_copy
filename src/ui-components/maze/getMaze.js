/* eslint-disable no-redeclare */

const Maze = (function () {
  function Maze (lx, ly, seed) {
    let bondHSize = (lx + 1) * ly
    let bondVSize = lx * (ly + 1)
    let pointSize = lx * ly
    let bondH = new Array(bondHSize)
    for (let i = 0; i < bondH.length; i++) {
      if (window.CP.shouldStopExecution(1)) { break }
      bondH[i] = false
    }
    window.CP.exitedLoop(1)

    let bondV = new Array(bondVSize)
    for (let i = 0; i < bondV.length; i++) {
      if (window.CP.shouldStopExecution(2)) { break }
      bondV[i] = false
    }
    window.CP.exitedLoop(2)

    let point = new Array(pointSize)
    for (let i = 0; i < point.length; i++) {
      if (window.CP.shouldStopExecution(3)) { break }
      point[i] = i
    }
    window.CP.exitedLoop(3)

    this.lx = lx
    this.ly = ly
    this.bondH = bondH
    this.bondV = bondV
    this.point = point
    this.makeMaze()
  }
  Maze.prototype.getClusterIndex = function (x, y) {
    let index = this.lx * y + x
    while (index !== this.point[index]) {
      if (window.CP.shouldStopExecution(4)) { break }
      index = this.point[index]
    }
    window.CP.exitedLoop(4)

    return index
  }
  Maze.prototype.connect = function (ix1, iy1, ix2, iy2) {
    let i1 = this.getClusterIndex(ix1, iy1)
    let i2 = this.getClusterIndex(ix2, iy2)
    if (i1 < i2) {
      this.point[i2] = i1
    } else {
      this.point[i1] = i2
    }
  }
  Maze.prototype.makeMazeSub = function (rate) {
        // make path horizontally
    for (let iy = 0; iy < this.ly; iy++) {
      if (window.CP.shouldStopExecution(6)) { break }
      for (let ix = 0; ix < this.lx - 1; ix++) {
        if (window.CP.shouldStopExecution(5)) { break }
        let rand = Math.random()
        if (rand < rate ||
                    this.getClusterIndex(ix, iy) === this.getClusterIndex(ix + 1, iy)) {
          continue
        }
        this.bondH[this.lx * iy + iy + ix + 1] = true
        this.connect(ix, iy, ix + 1, iy)
      }
      window.CP.exitedLoop(5)
    }
    window.CP.exitedLoop(6)

        // make path vertically
    for (let iy = 0; iy < this.ly - 1; iy++) {
      if (window.CP.shouldStopExecution(8)) { break }
      for (let ix = 0; ix < this.lx; ix++) {
        if (window.CP.shouldStopExecution(7)) { break }
        let rand = Math.random()
        if (rand < rate ||
                    this.getClusterIndex(ix, iy) === this.getClusterIndex(ix, iy + 1)) {
          continue
        }
        this.bondV[(iy + 1) * this.lx + ix] = true
        this.connect(ix, iy, ix, iy + 1)
      }
      window.CP.exitedLoop(7)
    }
    window.CP.exitedLoop(8)

    return
  }
  Maze.prototype.makeMaze = function () {
    for (let i = 0; i < 10; i++) {
      if (window.CP.shouldStopExecution(9)) { break }
      this.makeMazeSub(0.8)
    }
    window.CP.exitedLoop(9)

        // definitely connect unconnected clusters
    this.makeMazeSub(1.0)
        // open start and goal
    this.bondH[0] = true
    this.bondH[(this.lx + 1) * this.ly - 1] = true
    this.goal = {
      x: this.lx - 1,
      y: this.ly - 1
    }
    return
  }
  return Maze
}())
exports.__esModule = true
exports['default'] = Maze
