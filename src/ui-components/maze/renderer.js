const Renderer = (function () {
  function Renderer (ctx, unitWidth, unitHeight, offsetLeft, offsetTop) {
    this.ctx = ctx
    this.unitWidth = unitWidth
    this.unitHeight = unitHeight
    this.offsetLeft = offsetLeft
    this.offsetTop = offsetTop
    this.wallWidth = 2
  }
    // TODO: Review the boundary
  Renderer.prototype.clear = function (w, h) {
    this.ctx.clearRect(0, 0, w, h)
  }
  Renderer.prototype.setColor = function (fill, stroke) {
    this.ctx.fillStyle = fill
    this.ctx.strokeStyle = stroke
  }
  Renderer.prototype.beginPath = function () {
    this.ctx.beginPath()
  }
  Renderer.prototype.stroke = function () {
    this.ctx.stroke()
  }
  Renderer.prototype.drawImage = function (x, y, image) {
    let scaleX = this.unitWidth / image.width
    let scaleY = this.unitHeight / image.height
    let cx = x * this.unitWidth / scaleX + (this.offsetLeft / scaleX)
    let cy = y * this.unitHeight / scaleY + (this.offsetTop / scaleY)
    this.ctx.save()
    this.ctx.scale(scaleX, scaleY)
    this.ctx.imageSmoothingEnabled = false
    this.ctx.drawImage(image, cx, cy)
    this.ctx.restore()
  }
  Renderer.prototype.drawCircle = function (x, y, r) {
    this.ctx.beginPath()
    let cx = x * this.unitWidth + this.unitWidth / 2 + this.offsetLeft
    let cy = y * this.unitHeight + this.unitHeight / 2 + this.offsetTop
    r =
            r != null
                ? r
                : Math.min(this.unitWidth, this.unitHeight) / 2 - this.wallWidth
    this.ctx.arc(cx, cy, r, 0, 2 * Math.PI)
    this.ctx.fill()
    this.ctx.stroke()
  }
  Renderer.prototype.drawLine = function (x1, y1, x2, y2) {
    let fromX = this.offsetLeft + x1 * this.unitWidth
    let fromY = this.offsetTop + y1 * this.unitHeight
    let toX = this.offsetLeft + x2 * this.unitWidth
    let toY = this.offsetTop + y2 * this.unitHeight
    this.ctx.moveTo(fromX, fromY)
    this.ctx.lineTo(toX, toY)
  }
  Renderer.prototype.drawText = function (text, x, y) {
    let left = x * this.unitWidth + this.offsetLeft
    let top = y * this.unitHeight + this.offsetTop
    this.ctx.fillStyle = 'black'
    this.ctx.fillText(text, left, top)
  }
  return Renderer
}())
exports.__esModule = true
exports['default'] = Renderer
