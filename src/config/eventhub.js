/* eslint-disable camelcase */

import Vue from 'vue'
class EventBus {
  constructor () {
    this.vue = new Vue()
  }

    /**
     * Emit a given event to the bus channels
     *
     * @param {string} event
     * @param {object | null} data
     */
  fire (event, data = null) {
    this.vue.$emit(event, data)
  }
    /**
     * Register for the bus to fire a callback when an event occurs
     *
     * @param {string} event
     * @param {closure} callback
     */
  listen (event, callback) {
    this.vue.$on(event, callback)
  }
  /**
   * Register for the bus to fire a callback when an event occurs only for the first time
   *
   * @param {string} event
   * @param {closure} callback
   */
  listen_once (event, callback) {
    this.vue.$once(event, callback)
  }
  /**
   * Register for the bus to fire a callback when an event occurs only for the first time
   *
   * @param {string} event
   * @param {closure} callback
   */
    off (event) {
      this.vue.$off(event)
    }
    /**
     * Return a closure that will fire when called
     *
     * @param {string} event
     * @param {object} data
     * @returns {closure}
     */
  fireWhenReady (event, data = null) {
    return function () { this.fire(event, data) }.bind(this)
  }

  firePrimarySettings (event, data) {
    this.vue.$emit('uicomponentbgsettings_close')
    this.vue.$emit('uicomponenttext_close')
    this.vue.$emit('uicomponentimages_close')
    this.vue.$emit(event, data)
  }

  closePrimarySettings () {
    this.vue.$emit('uicomponentbgsettings_close')
    this.vue.$emit('uicomponenttext_close')
    this.vue.$emit('uicomponentimages_close')
  }
}

export default new EventBus()
