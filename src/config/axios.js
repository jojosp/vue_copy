import axios from 'axios'
const instance = axios.create({
  baseURL: process.env.APIENDPOINT,
  headers: { 'Cache-Control': 'no-cache', 'Content-Type': 'application/json' }
})
instance.defaults.withCredentials = true

export default instance
