import $ from 'jquery'
/* eslint-disable no-unused-vars */
/* eslint-disable no-tabs */
/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable indent */
/* eslint-disable no-undef */
/* eslint-disable camelcase */
/* eslint-disable semi */
/* eslint-disable space-before-blocks */
/* eslint-disable space-before-function-paren */
/* eslint-dsable space-in-parens */
/* eslint-disable keyword-spacing */
/* eslint-disable eqeqeq  */
/* eslint-disable space-in-parens */
/* eslint-disable quotes  */
/* eslint-disable space-infix-ops */
/* eslint-disable spaced-comment */
/* eslint-disable key-spacing */
/* eslint-disable comma-spacing */
/* eslint-disable one-var */
/* eslint-disable semi-spacing */
// Avoid `console` errors in browsers that lack a console.
(function () {
  var method
  var noop = function () {}
  var methods = [
    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
    'timeStamp', 'trace', 'warn'
  ]
  var length = methods.length
  var console = (window.console = window.console || {})

  while (length--) {
    method = methods[length]
        // Only stub undefined methods.
    if (!console[method]) {
      console[method] = noop
    }
  }
}())

// Place any jQuery/helper plugins in here.
function checkContentHeight (target) {
  var stageHeight = $(window).height()
  var newHeight = (stageHeight / 2) - (target.height() / 2)
  return newHeight
}

function checkContentWidth (target) {
  var stageWidth = $(window).width()
  var newWidth = (stageWidth / 2) - (target.width() / 2)
  return newWidth
}

function getDeviceVer () {
  var ua = navigator.userAgent
  var uaindex

	// determine OS
  if (ua.match(/(iPad|iPhone|iPod touch)/)) {
    userOS = 'iOS'
    uaindex = ua.indexOf('OS ')
  } else if (ua.match(/Android/)) {
    userOS = 'Android'
    uaindex = ua.indexOf('Android ')
  } else {
    userOS = 'unknown'
  }

	// determine version
  if (userOS === 'iOS' && uaindex > -1) {
    userOSver = ua.substr(uaindex + 3, 3).replace('_', '.')
  } else if (userOS === 'Android' && uaindex > -1) {
    userOSver = ua.substr(uaindex + 8, 3)
  } else {
    userOSver = 'unknown'
  }
  return Number(userOSver)
}

function shuffle (array) {
  var currentIndex = array.length,
	 temporaryValue,
	 randomIndex

	// While there remain elements to shuffle...
  while (currentIndex !== 0) {
		// Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex)
    currentIndex -= 1

		// And swap it with the current element.
    temporaryValue = array[currentIndex]
    array[currentIndex] = array[randomIndex]
    array[randomIndex] = temporaryValue
  }

  return array
}

function randomBoolean () {
  return Math.random() < 0.5
}

// //////////////////////////////////////////////////////////
// SOUND
// //////////////////////////////////////////////////////////
var enableMobileSound = true
var soundOn

function playSound (target, loop) {
  if (soundOn) {
    var isLoop
    if (loop) {
      isLoop = -1
      createjs.Sound.stop()
      musicLoop = createjs.Sound.play(target, createjs.Sound.INTERRUPT_NONE, 0, 0, isLoop, 1)
      if (musicLoop == null || musicLoop.playState == createjs.Sound.PLAY_FAILED) {
        return
      } else {
        musicLoop.removeAllEventListeners()
        musicLoop.addEventListener('complete', function (musicLoop) {

        })
      }
    } else {
      isLoop = 0
      createjs.Sound.play(target)
    }
  }
}

function stopSound () {
  createjs.Sound.stop()
}

/*!
 *
 * PLAY MUSIC - This is the function that runs to play and stop music
 *
 */
$.sound = {}
function playSoundLoop (sound) {
  if (soundOn) {
    if ($.sound[sound] == null) {
      $.sound[sound] = createjs.Sound.play(sound)
      $.sound[sound].removeAllEventListeners()
      $.sound[sound].addEventListener('complete', function () {
        $.sound[sound].play()
      })
    }
  }
}

function stopSoundLoop (sound) {
  if (soundOn) {
    if ($.sound[sound] != null) {
      $.sound[sound].stop()
      $.sound[sound] = null
    }
  }
}

function setSoundVolume (sound, vol) {
  if (soundOn) {
    if ($.sound[sound] != null) {
      $.sound[sound].volume = vol
    }
  }
}

/*!
 *
 * TOGGLE MUTE - This is the function that runs to toggle mute
 *
 */
function toggleMute (con) {
  createjs.Sound.setMute(con)
}
// //////////////////////////////////////////////////////////
// CANVAS
// //////////////////////////////////////////////////////////
var stage
var canvasW = 0
var canvasH = 0

/*!
 *
 * START GAME CANVAS - This is the function that runs to setup game canvas
 *
 */
function initGameCanvas (w, h) {
  var gameCanvas = document.getElementById('gameCanvas')
  gameCanvas.width = w
  gameCanvas.height = h

  canvasW = w
  canvasH = h
  stage = new createjs.Stage('gameCanvas')

  createjs.Touch.enable(stage)
  stage.enableMouseOver(20)
  stage.mouseMoveOutside = true

  createjs.Ticker.setFPS(60)
  createjs.Ticker.addEventListener('tick', tick)
}

var guide = false
var canvasContainer, mainContainer, selectContainer, thumbContainer, gameContainer, bgContainer, objectsContainer, resultContainer
var guideline, background, logo, buttonStart, bgSelect, selected, buttonPrev, buttonNext, bgTimer, timerTxt, bgCount, countTxt, bgComplete, resultScoreShadowTxt, resultScoreTxt, resultTitleShadowTxt, resultTitleTxt, buttonFacebook, buttonTwitter, buttonGoogle, buttonMain

$.puzzles = {}
$.objects = {}

/*!
 *
 * BUILD GAME CANVAS ASSERTS - This is the function that runs to build game canvas asserts
 *
 */
function buildGameCanvas () {
  canvasContainer = new createjs.Container()
  mainContainer = new createjs.Container()
  selectContainer = new createjs.Container()
  thumbContainer = new createjs.Container()
  bgContainer = new createjs.Container()
  objectsContainer = new createjs.Container()
  gameContainer = new createjs.Container()
  resultContainer = new createjs.Container()

  background = new createjs.Bitmap(loader.getResult('background'))
  logo = new createjs.Bitmap(loader.getResult('logo'))
  buttonStart = new createjs.Bitmap(loader.getResult('buttonStart'))
  centerReg(buttonStart)
  buttonStart.x = canvasW / 2
  buttonStart.y = canvasH / 100 * 75

  bgSelect = new createjs.Bitmap(loader.getResult('bgSelect'))
  buttonPrev = new createjs.Bitmap(loader.getResult('buttonPrev'))
  buttonNext = new createjs.Bitmap(loader.getResult('buttonNext'))
  centerReg(buttonPrev)
  centerReg(buttonNext)
  buttonPrev.x = canvasW / 100 * 20
  buttonPrev.y = canvasH / 100 * 80

  buttonNext.x = canvasW / 100 * 80
  buttonNext.y = canvasH / 100 * 80

  selected = new createjs.Bitmap(loader.getResult('selected'))
  centerReg(selected)

  var thumb_arr = [{x: canvasW / 100 * 25, y: canvasH / 100 * 45},
					{x: canvasW / 2, y: canvasH / 100 * 45},
					{x: canvasW / 100 * 75, y: canvasH / 100 * 45}]
  var thumbCount = 0

  for (n = 0; n < puzzles_arr.length; n++) {
    $.puzzles[n] = new createjs.Bitmap(loader.getResult('puzzle' + n))
    bgContainer.addChild($.puzzles[n])

    $.puzzles['thumb_' + n] = new createjs.Bitmap(loader.getResult('thumb' + n))
    centerReg($.puzzles['thumb_' + n])
    createHitarea($.puzzles['thumb_' + n])

    thumbContainer.addChild($.puzzles['thumb_' + n])
    $.puzzles['thumb_' + n].x = thumb_arr[thumbCount].x
    $.puzzles['thumb_' + n].y = thumb_arr[thumbCount].y

    thumbCount++
    if (thumbCount >= maxThumbPerPage) {
      thumbCount = 0
    }

    for (p = 0; p < puzzles_arr[n].objects.length; p++) {
      $.objects[n + '_' + p] = new createjs.Bitmap(loader.getResult('object_' + n + '_' + p))
      centerReg($.objects[n + '_' + p])
      objectsContainer.addChild($.objects[n + '_' + p])
    }
  }

  bgTimer = new createjs.Bitmap(loader.getResult('bgTimer'))
  centerReg(bgTimer)

  timerTxt = new createjs.Text()
  timerTxt.font = '50px granstander_cleanregular'
  timerTxt.color = '#fff'
  timerTxt.text = '00:00'
  timerTxt.textAlign = 'center'
  timerTxt.textBaseline = 'alphabetic'

  bgCount = new createjs.Bitmap(loader.getResult('bgCount'))
  centerReg(bgCount)

  countTxt = new createjs.Text()
  countTxt.font = '50px granstander_cleanregular'
  countTxt.color = '#fff'
  countTxt.text = '1/10'
  countTxt.textAlign = 'center'
  countTxt.textBaseline = 'alphabetic'

  bgCount.visible = countTxt.visible = counterDisplay

  bgComplete = new createjs.Bitmap(loader.getResult('bgComplete'))

	// result
  resultTitleTxt = new createjs.Text()
  resultTitleTxt.font = '80px granstander_cleanregular'
  resultTitleTxt.color = '#556270'
  resultTitleTxt.text = resultTitleText
  resultTitleTxt.textAlign = 'center'
  resultTitleTxt.textBaseline = 'alphabetic'
  resultTitleTxt.x = canvasW / 2
  resultTitleTxt.y = canvasH / 100 * 23

  resultTitleShadowTxt = new createjs.Text()
  resultTitleShadowTxt.font = '80px granstander_cleanregular'
  resultTitleShadowTxt.color = '#fff'
  resultTitleShadowTxt.text = resultTitleText
  resultTitleShadowTxt.textAlign = 'center'
  resultTitleShadowTxt.textBaseline = 'alphabetic'
  resultTitleShadowTxt.x = canvasW / 2
  resultTitleShadowTxt.y = resultTitleTxt.y + 10

  resultScoreTxt = new createjs.Text()
  resultScoreTxt.font = '130px granstander_cleanregular'
  resultScoreTxt.color = '#556270'
  resultScoreTxt.text = 100
  resultScoreTxt.textAlign = 'center'
  resultScoreTxt.textBaseline = 'alphabetic'
  resultScoreTxt.x = canvasW / 2
  resultScoreTxt.y = canvasH / 100 * 40

  resultScoreShadowTxt = new createjs.Text()
  resultScoreShadowTxt.font = '130px granstander_cleanregular'
  resultScoreShadowTxt.color = '#ffffff'
  resultScoreShadowTxt.text = 100
  resultScoreShadowTxt.textAlign = 'center'
  resultScoreShadowTxt.textBaseline = 'alphabetic'
  resultScoreShadowTxt.x = canvasW / 2
  resultScoreShadowTxt.y = resultScoreTxt.y + 10

  buttonMain = new createjs.Bitmap(loader.getResult('buttonMain'))
  centerReg(buttonMain)
  buttonMain.x = canvasW / 100 * 60
  buttonMain.y = canvasH / 100 * 78

  buttonNextGame = new createjs.Bitmap(loader.getResult('buttonNext'))
  centerReg(buttonNextGame)
  buttonNextGame.x = canvasW / 100 * 40
  buttonNextGame.y = canvasH / 100 * 78

  if (guide) {
    guideline = new createjs.Shape()
    guideline.graphics.setStrokeStyle(2).beginStroke('red').drawRect((stageW - contentW) / 2, (stageH - contentH) / 2, contentW, contentH)
  }
  mainContainer.addChild(logo, buttonStart)
  selectContainer.addChild(bgSelect, buttonPrev, buttonNext, thumbContainer, selected)
  gameContainer.addChild(bgContainer, objectsContainer, bgTimer, timerTxt, bgCount, countTxt, bgComplete)

  resultContainer.addChild(resultScoreShadowTxt, resultScoreTxt, resultTitleShadowTxt, resultTitleTxt, buttonFacebook, buttonTwitter, buttonGoogle, buttonMain, buttonNextGame)

  canvasContainer.addChild(background, mainContainer, selectContainer, gameContainer, resultContainer, guideline)
  stage.addChild(canvasContainer)

  resizeCanvas()
}

/*!
 *
 * RESIZE GAME CANVAS - This is the function that runs to resize game canvas
 *
 */
function resizeCanvas () {
 	if (canvasContainer != undefined) {
   bgTimer.x = canvasW - offset.x
   bgTimer.y = offset.y
   bgTimer.x -= 120
   bgTimer.y += 50

   timerTxt.x = bgTimer.x
   timerTxt.y = bgTimer.y + 20

   bgCount.x = offset.x
   bgCount.y = offset.y
   bgCount.x += 120
   bgCount.y += 50

   countTxt.x = bgCount.x
   countTxt.y = bgCount.y + 20
 }
}

/*!
 *
 * REMOVE GAME CANVAS - This is the function that runs to remove game canvas
 *
 */
function removeGameCanvas () {
	 stage.autoClear = true
	 stage.removeAllChildren()
	 stage.update()
	 createjs.Ticker.removeEventListener('tick', tick)
	 createjs.Ticker.removeEventListener('tick', stage)
}

/*!
 *
 * CANVAS LOOP - This is the function that runs for canvas loop
 *
 */
function tick (event) {
  updateGame()
  stage.update(event)
}

/*!
 *
 * CANVAS MISC FUNCTIONS
 *
 */
function centerReg (obj) {
  obj.regX = obj.image.naturalWidth / 2
  obj.regY = obj.image.naturalHeight / 2
}

function createHitarea (obj) {
  obj.hitArea = new createjs.Shape(new createjs.Graphics().beginFill('#000').drawRect(0, 0, obj.image.naturalWidth, obj.image.naturalHeight))
}
// //////////////////////////////////////////////////////////
// PUZZLES
// //////////////////////////////////////////////////////////

var puzzles_arr = [

  // puzzle 1
  {
    thumb: 'assets/puzzles/03/thumb.png',
    src: 'assets/puzzles/03/bg.png',
    objects: [{
      src: 'assets/puzzles/03/object_01.png',
      fixedX: 0,
      fixedY: 0,
      finalX: 324,
      finalY: 472
    },
      {
        src: 'assets/puzzles/03/object_02.png',
        fixedX: 0,
        fixedY: 0,
        finalX: 523,
        finalY: 444
      },
      {
        src: 'assets/puzzles/03/object_03.png',
        fixedX: 0,
        fixedY: 0,
        finalX: 706,
        finalY: 459
      },
      {
        src: 'assets/puzzles/03/object_04.png',
        fixedX: 0,
        fixedY: 0,
        finalX: 883,
        finalY: 429
      },
      {
        src: 'assets/puzzles/03/object_05.png',
        fixedX: 0,
        fixedY: 0,
        finalX: 1049,
        finalY: 462
      }
    ],
    drag: {
      mode: 'random',
      start: {
        x: 350,
        y: 550,
        width: 550,
        height: 50
      },
      area: {
        x: 128,
        y: 96,
        width: 1024,
        height: 576
      },
      range: 30,
      return: false
    },
    gravity: {
      status: false,
      ground: 600
    }
  },

  // puzzle 2
  {
    thumb: 'assets/puzzles/04/thumb.png',
    src: 'assets/puzzles/04/bg.png',
    objects: [{
      src: 'assets/puzzles/04/object_01.png',
      fixedX: 0,
      fixedY: 0,
      finalX: 335,
      finalY: 309
    },
      {
        src: 'assets/puzzles/04/object_02.png',
        fixedX: 0,
        fixedY: 0,
        finalX: 611,
        finalY: 305
      },
      {
        src: 'assets/puzzles/04/object_03.png',
        fixedX: 0,
        fixedY: 0,
        finalX: 955,
        finalY: 310
      },
      {
        src: 'assets/puzzles/04/object_04.png',
        fixedX: 0,
        fixedY: 0,
        finalX: 299,
        finalY: 516
      },
      {
        src: 'assets/puzzles/04/object_05.png',
        fixedX: 0,
        fixedY: 0,
        finalX: 559,
        finalY: 549
      },
      {
        src: 'assets/puzzles/04/object_06.png',
        fixedX: 0,
        fixedY: 0,
        finalX: 943,
        finalY: 522
      }
    ],
    drag: {
      mode: 'random',
      start: {
        x: 250,
        y: 400,
        width: 800,
        height: 100
      },
      area: {
        x: 128,
        y: 96,
        width: 1024,
        height: 576
      },
      range: 15,
      return: false
    },
    gravity: {
      status: true,
      ground: 650
    }
  },

  // puzzle 3
  {
    thumb: 'assets/puzzles/01/thumb.png',
    src: 'assets/puzzles/01/bg.png',
    objects: [{
      src: 'assets/puzzles/01/object_01.png',
      fixedX: 953,
      fixedY: 462,
      finalX: 253,
      finalY: 306
    },
      {
        src: 'assets/puzzles/01/object_02.png',
        fixedX: 1032,
        fixedY: 518,
        finalX: 230,
        finalY: 473
      },
      {
        src: 'assets/puzzles/01/object_03.png',
        fixedX: 943,
        fixedY: 415,
        finalX: 289,
        finalY: 460
      },
      {
        src: 'assets/puzzles/01/object_04.png',
        fixedX: 889,
        fixedY: 261,
        finalX: 411,
        finalY: 271
      },
      {
        src: 'assets/puzzles/01/object_05.png',
        fixedX: 1007,
        fixedY: 371,
        finalX: 411,
        finalY: 419
      },
      {
        src: 'assets/puzzles/01/object_06.png',
        fixedX: 896,
        fixedY: 327,
        finalX: 538,
        finalY: 288
      },
      {
        src: 'assets/puzzles/01/object_07.png',
        fixedX: 783,
        fixedY: 254,
        finalX: 625,
        finalY: 297
      },
      {
        src: 'assets/puzzles/01/object_08.png',
        fixedX: 978,
        fixedY: 257,
        finalX: 551,
        finalY: 398
      },
      {
        src: 'assets/puzzles/01/object_09.png',
        fixedX: 836,
        fixedY: 545,
        finalX: 542,
        finalY: 502
      },
      {
        src: 'assets/puzzles/01/object_10.png',
        fixedX: 826,
        fixedY: 343,
        finalX: 624,
        finalY: 453
      }
    ],
    drag: {
      mode: 'random',
      start: {
        x: 750,
        y: 250,
        width: 350,
        height: 300
      },
      area: {
        x: 128,
        y: 96,
        width: 1024,
        height: 576
      },
      range: 15,
      return: false
    },
    gravity: {
      status: false,
      ground: 600
    }
  },

  // puzzle 4
  {
    thumb: 'assets/puzzles/02/thumb.png',
    src: 'assets/puzzles/02/bg.png',
    objects: [{
      src: 'assets/puzzles/02/object_01.png',
      fixedX: 1083,
      fixedY: 418,
      finalX: 430,
      finalY: 158
    },
      {
        src: 'assets/puzzles/02/object_02.png',
        fixedX: 625,
        fixedY: 415,
        finalX: 429,
        finalY: 285
      },
      {
        src: 'assets/puzzles/02/object_03.png',
        fixedX: 902,
        fixedY: 431,
        finalX: 372,
        finalY: 268
      },
      {
        src: 'assets/puzzles/02/object_04.png',
        fixedX: 810,
        fixedY: 403,
        finalX: 370,
        finalY: 372
      },
      {
        src: 'assets/puzzles/02/object_05.png',
        fixedX: 1026,
        fixedY: 420,
        finalX: 490,
        finalY: 265
      },
      {
        src: 'assets/puzzles/02/object_06.png',
        fixedX: 941,
        fixedY: 458,
        finalX: 486,
        finalY: 373
      },
      {
        src: 'assets/puzzles/02/object_07.png',
        fixedX: 774,
        fixedY: 443,
        finalX: 402,
        finalY: 440
      },
      {
        src: 'assets/puzzles/02/object_08.png',
        fixedX: 723,
        fixedY: 408,
        finalX: 399,
        finalY: 573
      },
      {
        src: 'assets/puzzles/02/object_09.png',
        fixedX: 863,
        fixedY: 442,
        finalX: 455,
        finalY: 442
      },
      {
        src: 'assets/puzzles/02/object_10.png',
        fixedX: 982,
        fixedY: 416,
        finalX: 447,
        finalY: 576
      }
    ],
    drag: {
      mode: 'fixed',
      start: {
        x: 600,
        y: 400,
        width: 500,
        height: 100
      },
      area: {
        x: 128,
        y: 96,
        width: 1024,
        height: 576
      },
      range: 15,
      return: true
    },
    gravity: {
      status: false,
      ground: 600
    }
  },

  // puzzle 5
  {
    thumb: 'assets/puzzles/05/thumb.png',
    src: 'assets/puzzles/05/bg.png',
    objects: [{
      src: 'assets/puzzles/05/object_01.png',
      fixedX: 1083,
      fixedY: 418,
      finalX: 390,
      finalY: 217
    },
      {
        src: 'assets/puzzles/05/object_02.png',
        fixedX: 625,
        fixedY: 415,
        finalX: 560,
        finalY: 189
      },
      {
        src: 'assets/puzzles/05/object_03.png',
        fixedX: 902,
        fixedY: 431,
        finalX: 726,
        finalY: 216
      },
      {
        src: 'assets/puzzles/05/object_04.png',
        fixedX: 810,
        fixedY: 403,
        finalX: 869,
        finalY: 188
      },
      {
        src: 'assets/puzzles/05/object_05.png',
        fixedX: 1026,
        fixedY: 420,
        finalX: 416,
        finalY: 358
      },
      {
        src: 'assets/puzzles/05/object_06.png',
        fixedX: 941,
        fixedY: 458,
        finalX: 559,
        finalY: 358
      },
      {
        src: 'assets/puzzles/05/object_07.png',
        fixedX: 774,
        fixedY: 443,
        finalX: 728,
        finalY: 358
      },
      {
        src: 'assets/puzzles/05/object_08.png',
        fixedX: 723,
        fixedY: 408,
        finalX: 896,
        finalY: 358
      },
      {
        src: 'assets/puzzles/05/object_09.png',
        fixedX: 863,
        fixedY: 442,
        finalX: 389,
        finalY: 501
      },
      {
        src: 'assets/puzzles/05/object_10.png',
        fixedX: 982,
        fixedY: 416,
        finalX: 560,
        finalY: 527
      },
      {
        src: 'assets/puzzles/05/object_11.png',
        fixedX: 982,
        fixedY: 416,
        finalX: 726,
        finalY: 498
      },
      {
        src: 'assets/puzzles/05/object_12.png',
        fixedX: 982,
        fixedY: 416,
        finalX: 868,
        finalY: 526
      }
    ],
    drag: {
      mode: 'random',
      start: {
        x: 300,
        y: 200,
        width: 700,
        height: 300
      },
      area: {
        x: 128,
        y: 96,
        width: 1024,
        height: 576
      },
      range: 15,
      return: false
    },
    gravity: {
      status: false,
      ground: 600
    }
  }

]
// //////////////////////////////////////////////////////////
// GAME
// //////////////////////////////////////////////////////////

/*!
 *
 * GAME SETTING CUSTOMIZATION START
 *
 */
var counterDisplay = true // enable counter display
var resultTitleText = 'Best Time:' // text for result page title

/*!
 *
 * GAME SETTING CUSTOMIZATION END
 *
 */
$.editor = {
  enable: false
}
var playerData = {
  score: 0,
  type: ''
}
var gameData = {
  puzzleNum: 0,
  puzzeCount: 0,
  puzzleComplete: 0,
  gravity: false,
  timerCon: false,
  timer: '',
  timerDistance: 0
}
var gravityData = {
  gravity: 1.5,
  drag: 0.89,
  bounce: 0.4,
  ground: 768
}
/*!
 *
 * GAME BUTTONS - This is the function that runs to setup button event
 *
 */
function buildGameButton () {
  buttonStart.cursor = 'pointer'
  buttonStart.addEventListener('click', function (evt) {
    playSound('soundClick')
    goPage('select')
  })

  buttonPrev.cursor = 'pointer'
  buttonPrev.addEventListener('click', function (evt) {
    toggleSelect(false)
  })

  buttonNext.cursor = 'pointer'
  buttonNext.addEventListener('click', function (evt) {
    toggleSelect(true)
  })

  buttonMain.cursor = 'pointer'
  buttonMain.addEventListener('click', function (evt) {
    playSound('soundClick')
    goPage('main')
  })

  buttonNextGame.cursor = 'pointer'
  buttonNextGame.addEventListener('click', function (evt) {
    playSound('soundClick')
    prepareNextGame()
  })

  for (n = 0; n < puzzles_arr.length; n++) {
    $.puzzles['thumb_' + n].name = n
    $.puzzles['thumb_' + n].cursor = 'pointer'
    $.puzzles['thumb_' + n].selected = false
    $.puzzles['thumb_' + n].addEventListener('mousedown', function (evt) {
      selectCategoryThumbs(evt.target.name)
    })

    for (p = 0; p < puzzles_arr[n].objects.length; p++) {
      $.objects[n + '_' + p].clickNum = p
      buildDragAndDrop($.objects[n + '_' + p])
    }
  }
}

/*!
 *
 * SELECT CATEGARY - This is the function that runs to display select category
 *
 */
var selectPageNum = 1
var selectPageTotal = 1
var maxThumbPerPage = 3

function buildSelectPagination () {
  selectPageTotal = puzzles_arr.length / maxThumbPerPage
  if (String(selectPageTotal).indexOf('.') > -1) {
    selectPageTotal = Math.floor(selectPageTotal) + 1
  }
  toggleSelect(false)
}

function toggleSelect (con) {
  if (con) {
    selectPageNum++
    selectPageNum = selectPageNum > selectPageTotal ? selectPageTotal : selectPageNum
  } else {
    selectPageNum--
    selectPageNum = selectPageNum < 1 ? 1 : selectPageNum
  }
  selectPage(selectPageNum)
}

function selectPage (num) {
  selectPageNum = num
  selected.visible = false

  var startNum = (selectPageNum - 1) * maxThumbPerPage
  var endNum = startNum + (maxThumbPerPage - 1)

  for (n = 0; n < puzzles_arr.length; n++) {
    $.puzzles['thumb_' + n].selected = false
    if (n >= startNum && n <= endNum) {
      $.puzzles['thumb_' + n].visible = true
    } else {
      $.puzzles['thumb_' + n].visible = false
    }
  }

  if (selectPageNum == 1) {
    buttonPrev.visible = false
  } else {
    buttonPrev.visible = true
  }

  if (selectPageNum == selectPageTotal || selectPageTotal == 1) {
    buttonNext.visible = false
  } else {
    buttonNext.visible = true
  }
}

function selectCategoryThumbs (num) {
  playerData.type = num
  gameData.puzzleNum = num
  selected.x = $.puzzles['thumb_' + num].x
  selected.y = $.puzzles['thumb_' + num].y
  selected.visible = true

  if ($.puzzles['thumb_' + num].selected) {
    playSound('soundClick')
    goPage('game')
  } else {
    for (n = 0; n < puzzles_arr.length; n++) {
      $.puzzles['thumb_' + n].selected = false
    }
    $.puzzles['thumb_' + num].selected = true
  }
}

/*!
 *
 * DISPLAY PAGES - This is the function that runs to display pages
 *
 */
var curPage = ''

function goPage (page) {
  curPage = page

  mainContainer.visible = false
  selectContainer.visible = false
  gameContainer.visible = false
  resultContainer.visible = false

  var targetContainer = ''
  switch (page) {
    case 'main':
      targetContainer = mainContainer
      break

    case 'select':
      targetContainer = selectContainer
      break

    case 'game':
      targetContainer = gameContainer
      startGame()
      break

    case 'result':
      targetContainer = resultContainer
      playSound('soundEnd')
      stopGame()
      saveGame(playerData.score, playerData.type)
      break
  }

  targetContainer.alpha = 0
  targetContainer.visible = true
  $(targetContainer)
    .clearQueue()
    .stop(true, true)
    .animate({
      alpha: 1
    }, 500)
}

/*!
 *
 * START GAME - This is the function that runs to start play game
 *
 */
function startGame () {
  timerTxt.text = '00:00'
  bgComplete.alpha = 0

  loadPuzzles()
  toggleGameTimer(true)
  updateCounter()
}

function prepareNextGame () {
  gameData.puzzleNum++
  gameData.puzzleNum = gameData.puzzleNum > puzzles_arr.length - 1 ? 0 : gameData.puzzleNum
  goPage('game')
}

/*!
 *
 * STOP GAME - This is the function that runs to stop play game
 *
 */
function stopGame () {
  TweenMax.killAll(false, true, false)
}

/*!
 *
 * SAVE GAME - This is the function that runs to save game
 *
 */
function saveGame (score, type) {
  /* $.ajax({
      type: "POST",
      url: 'saveResults.php',
      data: {score:score},
      success: function (result) {
          console.log(result);
      }
    }); */
}

/*!
 *
 * GAME LOOP - This is the function that runs to loop game
 *
 */
function updateGame () {
  if (gameData.timerCon) {
    var nowDate = new Date()
    gameData.timerDistance = (nowDate.getTime() - gameData.timer.getTime())
    updateTimer()
  }

  if (gameData.gravity) {
    for (n = 0; n < puzzles_arr.length; n++) {
      for (p = 0; p < puzzles_arr[n].objects.length; p++) {
        var dragObject = $.objects[n + '_' + p]
        if (dragObject.active) {
          if (!dragObject.dragging) {
            // calculate new x and y position
            dragObject.y = dragObject.y + dragObject.yspeed
            dragObject.x = dragObject.x + dragObject.xspeed

            // bounce off the bottom of stage and reverse yspeed
            if (dragObject.y + dragObject.image.naturalHeight / 2 > gravityData.ground) {
              dragObject.y = gravityData.ground - dragObject.image.naturalHeight / 2
              dragObject.yspeed = -dragObject.yspeed * gravityData.bounce
            }

            // bounce off the top of stage and reverse yspeed
            if (dragObject.y - dragObject.image.naturalHeight / 2 < 0) {
              dragObject.y = dragObject.image.naturalHeight / 2
              dragObject.yspeed = -dragObject.yspeed * gravityData.bounce
            }

            // bounce off right of stage
            if (dragObject.x + dragObject.image.naturalWidth / 2 > canvasW) {
              dragObject.x = canvasW - dragObject.image.naturalWidth / 2
              dragObject.xspeed = -dragObject.xspeed * gravityData.bounce
            }
            // bounce off left of stage
            if (dragObject.x - dragObject.image.naturalWidth / 2 < 0) {
              dragObject.x = dragObject.image.naturalWidth / 2
              dragObject.xspeed = -dragObject.xspeed * gravityData.bounce
            }
            // recalculate x and y speeds figuring in drag (friction) and gravity for y
            dragObject.yspeed = dragObject.yspeed * gravityData.drag + gravityData.gravity
            dragObject.xspeed = dragObject.xspeed * gravityData.drag
          } else {
            dragObject.xspeed = dragObject.x - dragObject.oldx
            dragObject.yspeed = dragObject.y - dragObject.oldy
            dragObject.oldx = dragObject.x
            dragObject.oldy = dragObject.y
          }
        }
      }
    }
  }
}

/*!
 *
 * LOAD PUZZLES - This is the function that runs to load puzzles
 *
 */
function loadPuzzles () {
  gameData.puzzleCount = 0
  gameData.puzzleComplete = puzzles_arr[gameData.puzzleNum].objects.length
  checkEnableGravity()

  for (n = 0; n < puzzles_arr.length; n++) {
    $.puzzles[n].visible = false
    if (gameData.puzzleNum == n) {
      $.puzzles[n].visible = true
    }

    for (p = 0; p < puzzles_arr[n].objects.length; p++) {
      $.objects[n + '_' + p].visible = false
      toggleObjActive($.objects[n + '_' + p], false)
      if (gameData.puzzleNum == n) {
        $.objects[n + '_' + p].visible = true
        toggleObjActive($.objects[n + '_' + p], true)
      }
    }
  }

  setObjectsPosition()
}

/*!
 *
 * CHECK GRAVITY OPTION - This is the function that runs to check gravity option
 *
 */
function checkEnableGravity () {
  gameData.gravity = false

  if (puzzles_arr[gameData.puzzleNum].drag.mode != 'fixed') {
    gameData.gravity = puzzles_arr[gameData.puzzleNum].gravity.status
    gravityData.ground = puzzles_arr[gameData.puzzleNum].gravity.ground
  }
}

/*!
 *
 * SET DRAG OBJECTS POSITION - This is the function that runs set drag objects position
 *
 */
function setObjectsPosition () {
  for (n = 0; n < puzzles_arr.length; n++) {
    for (p = 0; p < puzzles_arr[n].objects.length; p++) {
      if (gameData.puzzleNum == n) {
        console.log(puzzles_arr[n].drag.mode)
        if (puzzles_arr[n].drag.mode == 'fixed') {
          if (puzzles_arr[n].objects[p].fixedX != undefined) {
            var fixedX = puzzles_arr[n].objects[p].fixedX
            var fixedY = puzzles_arr[n].objects[p].fixedY

            fixedX = fixedX == 0 ? canvasW / 2 : fixedX
            fixedY = fixedY == 0 ? canvasH / 2 : fixedY
            $.objects[n + '_' + p].x = fixedX
            $.objects[n + '_' + p].y = fixedY
          }
        } else {
          randomizePosition($.objects[n + '_' + p])
        }
      }
    }
  }
}

function setObjectsFinalPosition () {
  for (n = 0; n < puzzles_arr.length; n++) {
    for (p = 0; p < puzzles_arr[n].objects.length; p++) {
      if (gameData.puzzleNum == n) {
        if (puzzles_arr[n].objects[p].finalX != undefined) {
          $.objects[n + '_' + p].x = puzzles_arr[n].objects[p].finalX
          $.objects[n + '_' + p].y = puzzles_arr[n].objects[p].finalY
        }
      }
    }
  }
}

function randomizePosition (obj) {
  obj.x = puzzles_arr[gameData.puzzleNum].drag.start.x + Math.floor(Math.random() * (puzzles_arr[gameData.puzzleNum].drag.start.width))
  obj.y = puzzles_arr[gameData.puzzleNum].drag.start.y + Math.floor(Math.random() * (puzzles_arr[gameData.puzzleNum].drag.start.height))
}

/*!
 *
 * SETUP OBJECTS EVENTS - This is the function that runs to setup objects events
 *
 */
function buildDragAndDrop (obj) {
  obj.addEventListener('mousedown', function (evt) {
    toggleDragEvent(evt, 'drag')
  })
  obj.addEventListener('pressmove', function (evt) {
    toggleDragEvent(evt, 'move')
  })
  obj.addEventListener('pressup', function (evt) {
    toggleDragEvent(evt, 'release')
  })
}

function toggleObjActive (obj, con) {
  if (con) {
    obj.active = true
    obj.cursor = 'pointer'
    obj.dragging = false
    obj.yspeed = 0
    obj.xspeed = 0
  } else {
    obj.active = false
    obj.cursor = 'default'
    obj.dragging = false
    obj.yspeed = 0
    obj.xspeed = 0
  }
}

function toggleDragEvent (obj, con) {
  if (obj.target.active) {
    switch (con) {
      case 'drag':
        playSound('soundSelect')

        obj.target.dragging = true
        setChildIndex(obj.target, true)
        obj.target.offset = {
          x: obj.target.x - (obj.stageX),
          y: obj.target.y - (obj.stageY)
        }
        checkWithinArea(obj.target)
        break

      case 'move':
        obj.target.x = (obj.stageX) + obj.target.offset.x
        obj.target.y = (obj.stageY) + obj.target.offset.y
        checkWithinArea(obj.target)
        break

      case 'release':
        obj.target.dragging = false
        if ($.editor.enable) {
          updateArray('objects')
        } else {
          checkDropArea(obj.target)
        }
        break
    }
  }
}

/*!
 *
 * DRAG WITHIN AREA - This is the function that runs to drag within area
 *
 */
function checkWithinArea (obj) {
  var startX = puzzles_arr[gameData.puzzleNum].drag.area.x
  var startY = puzzles_arr[gameData.puzzleNum].drag.area.y
  var endX = puzzles_arr[gameData.puzzleNum].drag.area.x + puzzles_arr[gameData.puzzleNum].drag.area.width
  var endY = puzzles_arr[gameData.puzzleNum].drag.area.y + puzzles_arr[gameData.puzzleNum].drag.area.height

  if (obj.x - (obj.image.naturalWidth / 2) <= startX) {
    obj.x = startX + (obj.image.naturalWidth / 2)
  }

  if (obj.x + (obj.image.naturalWidth / 2) >= endX) {
    obj.x = endX - (obj.image.naturalWidth / 2)
  }

  if (obj.y - (obj.image.naturalHeight / 2) <= startY) {
    obj.y = startY + (obj.image.naturalHeight / 2)
  }

  if (obj.y + (obj.image.naturalHeight / 2) >= endY) {
    obj.y = endY - (obj.image.naturalHeight / 2)
  }
}

/*!
 *
 * CHECK DROP AREA - This is the function that runs to check drop area
 *
 */
function checkDropArea (obj) {
  var finalX = puzzles_arr[gameData.puzzleNum].objects[obj.clickNum].finalX
  var finalY = puzzles_arr[gameData.puzzleNum].objects[obj.clickNum].finalY
  var dropX = false
  var dropY = false

  if (obj.x > (finalX - puzzles_arr[gameData.puzzleNum].drag.range) && obj.x < (finalX + puzzles_arr[gameData.puzzleNum].drag.range)) {
    dropX = true
  }
  if (obj.y > (finalY - puzzles_arr[gameData.puzzleNum].drag.range) && obj.y < (finalY + puzzles_arr[gameData.puzzleNum].drag.range)) {
    dropY = true
  }
  if (dropX && dropY) {
    playSound('soundCorrect')

    gameData.puzzleCount++
    setChildIndex(obj, false)
    toggleObjActive(obj, false)
    TweenMax.to(obj, 0.3, {
      x: finalX,
      y: finalY,
      overwrite: true
    })
    checkEndGame()
    updateCounter()
  } else {
    playSound('soundWrong')

    if (puzzles_arr[gameData.puzzleNum].drag.return) {
      TweenMax.to(obj, 0.5, {
        x: puzzles_arr[gameData.puzzleNum].objects[obj.clickNum].fixedX,
        y: puzzles_arr[gameData.puzzleNum].objects[obj.clickNum].fixedY,
        overwrite: true
      })
    }
  }
}

/*!
 *
 * CHECK END GAME - This is the function that runs to check eng game
 *
 */
function checkEndGame () {
  if (gameData.puzzleCount == gameData.puzzleComplete) {
    toggleGameTimer(false)

    playSound('soundComplete')
    TweenMax.to(bgComplete, 0.5, {
      delay: 1,
      alpha: 1,
      overwrite: true,
      onComplete: function () {
        TweenMax.to(bgComplete, 2.5, {
          overwrite: true,
          onComplete: function () {
            goPage('result')
          }
        })
      }
    })
  }
}

/*!
 *
 * CHILD INDEX - This is the function that runs to set child index
 *
 */
function setChildIndex (obj, con) {
  if (con) {
    objectsContainer.setChildIndex(obj, objectsContainer.getNumChildren() - 1)
  } else {
    objectsContainer.setChildIndex(obj, 0)
  }
}

/*!
 *
 * TOGGLE GAME TIMER - This is the function that runs to toggle game timer
 *
 */
function toggleGameTimer (con) {
  if (con) {
    gameData.timer = new Date()
  } else {

  }
  gameData.timerCon = con
}

function updateTimer () {
  playerData.score = gameData.timerDistance
  timerTxt.text = millisecondsToTime(gameData.timerDistance)

  resultScoreTxt.text = millisecondsToTime(gameData.timerDistance)
  resultScoreShadowTxt.text = millisecondsToTime(gameData.timerDistance)
}

function millisecondsToTime (milli) {
  var milliseconds = milli % 1000
  var seconds = Math.floor((milli / 1000) % 60)
  var minutes = Math.floor((milli / (60 * 1000)) % 60)

  if (seconds < 10) {
    seconds = '0' + seconds
  }

  if (minutes < 10) {
    minutes = '0' + minutes
  }
  // return minutes +':'+ seconds + ':' + formatDigit(milliseconds, 2);
  return minutes + ':' + seconds
}

function updateCounter () {
  countTxt.text = gameData.puzzleCount + '/' + gameData.puzzleComplete
}
// //////////////////////////////////////////////////////////
// MOBILE
// //////////////////////////////////////////////////////////
var rotateInstruction = true // enable rotate instruction for mobile
var forPortrait = false // for portrait only, set false for landscape mode

/*!
 *
 * START MOBILE CHECK - This is the function that runs for mobile event
 *
 */
function checkMobileEvent () {
  if ($.browser.mobile || isTablet) {
    $('body').on('touchmove', function (e) {
      e.preventDefault()
      e.stopPropagation()
      return false
    })

    if (!rotateInstruction) {
      $('#canvasHolder').show()
      $('#rotateHolder').hide()
      return
    }

    $(window).off('orientationchange').on('orientationchange', function (event) {
      $('#canvasHolder').hide()
      $('#rotateHolder').hide()

      setTimeout(function () {
        checkMobileOrientation()
      }, 1000)
    })
    checkMobileOrientation()
  }
}

/*!
 *
 * MOBILE ORIENTATION CHECK - This is the function that runs to check mobile orientation
 *
 */
function checkMobileOrientation () {
  var o = window.orientation
  var isLandscape = false

  if (window.innerWidth > window.innerHeight) {
    isLandscape = true
  }

  var display = false
  if (!isLandscape) {
		// Portrait
    if (forPortrait) {
      display = true
    }
  } else {
		// Landscape
    if (!forPortrait) {
      display = true
    }
  }

  if (!display) {
    toggleRotate(true)
  } else {
    toggleRotate(false)
    $('#canvasHolder').show()
  }
}

/*!
 *
 * TOGGLE ROTATE MESSAGE - This is the function that runs to display/hide rotate instruction
 *
 */
function toggleRotate (con) {
  if (con) {
    $('#rotateHolder').fadeIn()
  } else {
    $('#rotateHolder').fadeOut()
  }
}
// //////////////////////////////////////////////////////////
// MAIN
// //////////////////////////////////////////////////////////
var stageW = 1280
var stageH = 768
var contentW = 1024
var contentH = 576

/*!
 *
 * START BUILD GAME - This is the function that runs build game
 *
 */
// function initMain () {
//   if (!$.browser.mobile || !isTablet) {
//     $('#canvasHolder').show()
//   }
//
//   initGameCanvas(stageW, stageH)
//   buildGameCanvas()
//   buildGameButton()
//   buildSelectPagination()
//
//   if ($.editor.enable) {
//     loadEditPage()
//     goPage('game')
//   } else {
//     playSoundLoop('musicGame')
//     goPage('main')
//   }
//   resizeCanvas()
// }

var windowW = windowH = 0
var scalePercent = 0
var offset = {x: 0, y: 0, left: 0, top: 0}

/*!
 *
 * GAME RESIZE - This is the function that runs to resize and centralize the game
 *
 */
function resizeGameFunc () {
  setTimeout(function () {
    $('.mobileRotate').css('left', checkContentWidth($('.mobileRotate')))
    $('.mobileRotate').css('top', checkContentHeight($('.mobileRotate')))

    windowW = window.innerWidth
    windowH = window.innerHeight

    scalePercent = windowW / contentW
    if ((contentH * scalePercent) > windowH) {
      scalePercent = windowH / contentH
    }

    scalePercent = scalePercent > 1 ? 1 : scalePercent

    if (windowW > stageW && windowH > stageH) {
      if (windowW > stageW) {
        scalePercent = windowW / stageW
        if ((stageH * scalePercent) > windowH) {
          scalePercent = windowH / stageH
        }
      }
    }

    var newCanvasW = ((stageW) * scalePercent)
    var newCanvasH = ((stageH) * scalePercent)

    offset.left = 0
    offset.top = 0

    if (newCanvasW > windowW) {
      offset.left = -((newCanvasW) - windowW)
    } else {
      offset.left = windowW - (newCanvasW)
    }

    if (newCanvasH > windowH) {
      offset.top = -((newCanvasH) - windowH)
    } else {
      offset.top = windowH - (newCanvasH)
    }

    offset.x = 0
    offset.y = 0

    if (offset.left < 0) {
      offset.x = Math.abs((offset.left / scalePercent) / 2)
    }
    if (offset.top < 0) {
      offset.y = Math.abs((offset.top / scalePercent) / 2)
    }

    $('canvas').css('width', newCanvasW)
    $('canvas').css('height', newCanvasH)

    $('canvas').css('left', (offset.left / 2))
    $('canvas').css('top', (offset.top / 2))

    $(window).scrollTop(0)

    resizeCanvas()
  }, 100)
}
// //////////////////////////////////////////////////////////
// CANVAS LOADER
// //////////////////////////////////////////////////////////

 /*!
 *
 * START CANVAS PRELOADER - This is the function that runs to preload canvas asserts
 *
 */
function initPreload () {
  toggleLoader(true)

  checkMobileEvent()

  $(window).resize(function () {
    resizeGameFunc()
  })
  resizeGameFunc()

  loader = new createjs.LoadQueue(false)
  manifest = [
			{src: 'assets/background.png', id: 'background'},
			{src: 'assets/logo.png', id: 'logo'},
			{src: 'assets/button_start.png', id: 'buttonStart'},
			{src: 'assets/bg_select.png', id: 'bgSelect'},
			{src: 'assets/selected.png', id: 'selected'},
			{src: 'assets/button_next.png', id: 'buttonNext'},
			{src: 'assets/button_prev.png', id: 'buttonPrev'},
			{src: 'assets/bg_timer.png', id: 'bgTimer'},
			{src: 'assets/bg_count.png', id: 'bgCount'},
			{src: 'assets/bg_complete.png', id: 'bgComplete'},
			{src: 'assets/button_main.png', id: 'buttonMain'},
			{src: 'assets/button_facebook.png', id: 'buttonFacebook'},
			{src: 'assets/button_twitter.png', id: 'buttonTwitter'},
			{src: 'assets/button_google.png', id: 'buttonGoogle'}]

  for (n = 0; n < puzzles_arr.length; n++) {
    manifest.push({src: puzzles_arr[n].src, id: 'puzzle' + n})
    manifest.push({src: puzzles_arr[n].thumb, id: 'thumb' + n})

    for (p = 0; p < puzzles_arr[n].objects.length; p++) {
      manifest.push({src: puzzles_arr[n].objects[p].src, id: 'object_' + n + '_' + p})
    }
  }

  soundOn = true
  if ($.browser.mobile || isTablet) {
    if (!enableMobileSound) {
      soundOn = false
    }
  }

  if (soundOn) {
    manifest.push({src: 'assets/sounds/click.ogg', id: 'soundClick'})
    manifest.push({src: 'assets/sounds/complete.ogg', id: 'soundComplete'})
    manifest.push({src: 'assets/sounds/correct.ogg', id: 'soundCorrect'})
    manifest.push({src: 'assets/sounds/error.ogg', id: 'soundWrong'})
    manifest.push({src: 'assets/sounds/music.ogg', id: 'musicGame'})
    manifest.push({src: 'assets/sounds/select.ogg', id: 'soundSelect'})

    createjs.Sound.alternateExtensions = ['mp3']
    loader.installPlugin(createjs.Sound)
  }

  loader.addEventListener('complete', handleComplete)
  loader.addEventListener('fileload', fileComplete)
  loader.addEventListener('error', handleFileError)
  loader.on('progress', handleProgress, this)
  loader.loadManifest(manifest)
}

/*!
 *
 * CANVAS FILE COMPLETE EVENT - This is the function that runs to update when file loaded complete
 *
 */
function fileComplete (evt) {
  var item = evt.item
	// console.log("Event Callback file loaded ", evt.item.id);
}

/*!
 *
 * CANVAS FILE HANDLE EVENT - This is the function that runs to handle file error
 *
 */
function handleFileError (evt) {
  console.log('error ', evt)
}

/*!
 *
 * CANVAS PRELOADER UPDATE - This is the function that runs to update preloder progress
 *
 */
function handleProgress () {
  $('#mainLoader span').html(Math.round(loader.progress / 1 * 100) + '%')
}

/*!
 *
 * CANVAS PRELOADER COMPLETE - This is the function that runs when preloader is complete
 *
 */
function handleComplete () {
  toggleLoader(false)
  initMain()
};

/*!
 *
 * TOGGLE LOADER - This is the function that runs to display/hide loader
 *
 */
function toggleLoader (con) {
  if (con) {
    $('#mainLoader').show()
  } else {
    $('#mainLoader').hide()
  }
}
// //////////////////////////////////////////////////////////
// INIT
// //////////////////////////////////////////////////////////
var stageWidth, stageHeight = 0
var isLoaded = false

 /*!
 *
 * DOCUMENT READY
 *
 */
$(function () {
	// Check for running exported on file protocol
  if (window.location.protocol.substr(0, 4) === 'file') {
    alert("To install the game just upload folder 'game' to your server. The game won't run locally with some browser like Chrome due to some security mode.")
  }

  $(window).resize(function () {
    resizeLoaderFunc()
  })
  resizeLoaderFunc()
  checkBrowser()
})

/*!
 *
 * LOADER RESIZE - This is the function that runs to centeralised loader when resize
 *
 */
function resizeLoaderFunc () {
  stageWidth = $(window).width()
  stageHeight = $(window).height()

  $('#mainLoader').css('left', checkContentWidth($('#mainLoader')))
  $('#mainLoader').css('top', checkContentHeight($('#mainLoader')))
}

/*!
 *
 * BROWSER DETECT - This is the function that runs for browser and feature detection
 *
 */
var browserSupport = false
var isTablet
function checkBrowser () {
  isTablet = (/ipad|android|android 3.0|xoom|sch-i800|playbook|tablet|kindle/i.test(navigator.userAgent.toLowerCase()))
  deviceVer = getDeviceVer()

  var canvasEl = document.createElement('canvas')
  if (canvasEl.getContext) {
	  browserSupport = true
  }

  if (browserSupport) {
    if (!isLoaded) {
      isLoaded = true
      initPreload()
    }
  } else {
		// browser not support
    $('#notSupportHolder').show()
  }
}
