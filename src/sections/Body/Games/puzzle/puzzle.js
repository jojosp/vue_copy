// //////////////////////////////////////////////////////////
// PUZZLES
// //////////////////////////////////////////////////////////

const puzzles_arr = [

					// puzzle 1
  {thumb: 'assets/puzzles/03/thumb.png',
    src: 'assets/puzzles/03/bg.png',
    objects: [{src: 'assets/puzzles/03/object_01.png', fixedX: 0, fixedY: 0, finalX: 324, finalY: 472},
							{src: 'assets/puzzles/03/object_02.png', fixedX: 0, fixedY: 0, finalX: 523, finalY: 444},
							{src: 'assets/puzzles/03/object_03.png', fixedX: 0, fixedY: 0, finalX: 706, finalY: 459},
							{src: 'assets/puzzles/03/object_04.png', fixedX: 0, fixedY: 0, finalX: 883, finalY: 429},
							{src: 'assets/puzzles/03/object_05.png', fixedX: 0, fixedY: 0, finalX: 1049, finalY: 462}],
    drag: {mode: 'random',
      start: {x: 350, y: 550, width: 550, height: 50},
      area: {x: 128, y: 96, width: 1024, height: 576},
      range: 30,
      return: false},
    gravity: {status: false, ground: 600}
  },

					// puzzle 2
  {thumb: 'assets/puzzles/04/thumb.png',
    src: 'assets/puzzles/04/bg.png',
    objects: [{src: 'assets/puzzles/04/object_01.png', fixedX: 0, fixedY: 0, finalX: 335, finalY: 309},
							{src: 'assets/puzzles/04/object_02.png', fixedX: 0, fixedY: 0, finalX: 611, finalY: 305},
							{src: 'assets/puzzles/04/object_03.png', fixedX: 0, fixedY: 0, finalX: 955, finalY: 310},
							{src: 'assets/puzzles/04/object_04.png', fixedX: 0, fixedY: 0, finalX: 299, finalY: 516},
							{src: 'assets/puzzles/04/object_05.png', fixedX: 0, fixedY: 0, finalX: 559, finalY: 549},
							{src: 'assets/puzzles/04/object_06.png', fixedX: 0, fixedY: 0, finalX: 943, finalY: 522}],
    drag: {mode: 'random',
      start: {x: 250, y: 400, width: 800, height: 100},
      area: {x: 128, y: 96, width: 1024, height: 576},
      range: 15,
      return: false},
    gravity: {status: true, ground: 650}
  },

					// puzzle 3
  {thumb: 'assets/puzzles/01/thumb.png',
    src: 'assets/puzzles/01/bg.png',
    objects: [{src: 'assets/puzzles/01/object_01.png', fixedX: 953, fixedY: 462, finalX: 253, finalY: 306},
							{src: 'assets/puzzles/01/object_02.png', fixedX: 1032, fixedY: 518, finalX: 230, finalY: 473},
							{src: 'assets/puzzles/01/object_03.png', fixedX: 943, fixedY: 415, finalX: 289, finalY: 460},
							{src: 'assets/puzzles/01/object_04.png', fixedX: 889, fixedY: 261, finalX: 411, finalY: 271},
							{src: 'assets/puzzles/01/object_05.png', fixedX: 1007, fixedY: 371, finalX: 411, finalY: 419},
							{src: 'assets/puzzles/01/object_06.png', fixedX: 896, fixedY: 327, finalX: 538, finalY: 288},
							{src: 'assets/puzzles/01/object_07.png', fixedX: 783, fixedY: 254, finalX: 625, finalY: 297},
							{src: 'assets/puzzles/01/object_08.png', fixedX: 978, fixedY: 257, finalX: 551, finalY: 398},
							{src: 'assets/puzzles/01/object_09.png', fixedX: 836, fixedY: 545, finalX: 542, finalY: 502},
							{src: 'assets/puzzles/01/object_10.png', fixedX: 826, fixedY: 343, finalX: 624, finalY: 453}],
    drag: {mode: 'random',
      start: {x: 750, y: 250, width: 350, height: 300},
      area: {x: 128, y: 96, width: 1024, height: 576},
      range: 15,
      return: false},
    gravity: {status: false, ground: 600}
  },

					// puzzle 4
  {thumb: 'assets/puzzles/02/thumb.png',
    src: 'assets/puzzles/02/bg.png',
    objects: [{src: 'assets/puzzles/02/object_01.png', fixedX: 1083, fixedY: 418, finalX: 430, finalY: 158},
							{src: 'assets/puzzles/02/object_02.png', fixedX: 625, fixedY: 415, finalX: 429, finalY: 285},
							{src: 'assets/puzzles/02/object_03.png', fixedX: 902, fixedY: 431, finalX: 372, finalY: 268},
							{src: 'assets/puzzles/02/object_04.png', fixedX: 810, fixedY: 403, finalX: 370, finalY: 372},
							{src: 'assets/puzzles/02/object_05.png', fixedX: 1026, fixedY: 420, finalX: 490, finalY: 265},
							{src: 'assets/puzzles/02/object_06.png', fixedX: 941, fixedY: 458, finalX: 486, finalY: 373},
							{src: 'assets/puzzles/02/object_07.png', fixedX: 774, fixedY: 443, finalX: 402, finalY: 440},
							{src: 'assets/puzzles/02/object_08.png', fixedX: 723, fixedY: 408, finalX: 399, finalY: 573},
							{src: 'assets/puzzles/02/object_09.png', fixedX: 863, fixedY: 442, finalX: 455, finalY: 442},
							{src: 'assets/puzzles/02/object_10.png', fixedX: 982, fixedY: 416, finalX: 447, finalY: 576}],
    drag: {mode: 'fixed',
      start: {x: 600, y: 400, width: 500, height: 100},
      area: {x: 128, y: 96, width: 1024, height: 576},
      range: 15,
      return: true},
    gravity: {status: false, ground: 600}
  },

					// puzzle 5
  {thumb: 'assets/puzzles/05/thumb.png',
    src: 'assets/puzzles/05/bg.png',
    objects: [{src: 'assets/puzzles/05/object_01.png', fixedX: 1083, fixedY: 418, finalX: 390, finalY: 217},
							{src: 'assets/puzzles/05/object_02.png', fixedX: 625, fixedY: 415, finalX: 560, finalY: 189},
							{src: 'assets/puzzles/05/object_03.png', fixedX: 902, fixedY: 431, finalX: 726, finalY: 216},
							{src: 'assets/puzzles/05/object_04.png', fixedX: 810, fixedY: 403, finalX: 869, finalY: 188},
							{src: 'assets/puzzles/05/object_05.png', fixedX: 1026, fixedY: 420, finalX: 416, finalY: 358},
							{src: 'assets/puzzles/05/object_06.png', fixedX: 941, fixedY: 458, finalX: 559, finalY: 358},
							{src: 'assets/puzzles/05/object_07.png', fixedX: 774, fixedY: 443, finalX: 728, finalY: 358},
							{src: 'assets/puzzles/05/object_08.png', fixedX: 723, fixedY: 408, finalX: 896, finalY: 358},
							{src: 'assets/puzzles/05/object_09.png', fixedX: 863, fixedY: 442, finalX: 389, finalY: 501},
							{src: 'assets/puzzles/05/object_10.png', fixedX: 982, fixedY: 416, finalX: 560, finalY: 527},
							{src: 'assets/puzzles/05/object_11.png', fixedX: 982, fixedY: 416, finalX: 726, finalY: 498},
							{src: 'assets/puzzles/05/object_12.png', fixedX: 982, fixedY: 416, finalX: 868, finalY: 526}],
    drag: {mode: 'random',
      start: {x: 300, y: 200, width: 700, height: 300},
      area: {x: 128, y: 96, width: 1024, height: 576},
      range: 15,
      return: false},
    gravity: {status: false, ground: 600}
  }

]
export default puzzles_arr;
