export default {
  data () {
    return {
      gameData: {
        puzzleNum: 0,
        puzzeCount: 0,
        puzzleComplete: 0,
        gravity: false,
        timerCon: false,
        timer: '',
        timerDistance: 0
      },
      gravityData: {
        gravity: 1.5,
        drag: 0.89,
        bounce: 0.4,
        ground: 768
      },
      layerData: {
        score: 0,
        type: ''
      }
    }
  }
}
