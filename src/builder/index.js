import Dropzone from './Dropzone'
import Component from './Component'
import Btoolbar from './BToolbar'
import Blocks from './Blocks'
import SkillAssignment from './SkillAssignment'
import BugReportForm from './BugReportForm'

const components = {
  Dropzone,
  Component,
  Blocks,
  Btoolbar,
  SkillAssignment,
  BugReportForm
}

components.install = (Vue) => {
  for (const componentName in components) {
    const component = components[componentName]

    if (component && componentName !== 'install') {
      Vue.component(component.name, component)
    }
  }
}
export default components
