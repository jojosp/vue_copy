const DataLoader = {
  data () {
    return {
      isDataLoaded: false
    }
  },
  methods: {
    resolveDataLoader () {
      this.isDataLoaded = true
    },
    resetDataLoader () {
      this.isDataLoaded = false
    }
  }
}
export default DataLoader
