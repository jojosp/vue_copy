CREATE DEFINER=`georgepan`@`%` PROCEDURE `clearassetsbeta`(IN packageid INT, IN modulesvarchar VARCHAR(255))
BEGIN
DECLARE a_json JSON;
DECLARE existflag INT DEFAULT 0;
DECLARE count1  INT;
DECLARE count2  INT;
DECLARE countpages  INT;
DECLARE countmedia  INT;
DECLARE jsonsearch VARCHAR(255);
DECLARE medianame VARCHAR(255);
DECLARE medianotused VARCHAR(255);
DECLARE allmedia VARCHAR(255);
DECLARE done BOOLEAN DEFAULT FALSE;

DECLARE cur1 CURSOR FOR SELECT * FROM pjsons;
-- DECLARE cur1 CURSOR FOR EXECUTE stmt2;
-- DECLARE cur2 CURSOR FOR SELECT * FROM vw_myproc2;
DECLARE cur2 CURSOR FOR SELECT filename FROM media WHERE package_id = packageid;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

SET @select2 = CONCAT('CREATE OR REPLACE VIEW pjsons as SELECT page_json FROM pages WHERE module_id IN (', modulesvarchar, ')');
PREPARE stm2 FROM @select2;
EXECUTE stm2;
DEALLOCATE PREPARE stm2;

-- SET @sql = CONCAT('SELECT COUNT(*) FROM pages WHERE module_id IN (', modulesvarchar, ')');
-- SET @sql2 = CONCAT('SELECT page_json FROM pages WHERE module_id IN (', modulesvarchar, ')');
-- PREPARE stmt2 FROM @sql2;
-- PREPARE stmt FROM @sql;
-- DECLARE cur1 CURSOR FOR EXECUTE stmt2;
-- DECLARE cur2 CURSOR FOR SELECT filename FROM media WHERE package_id = packageid;
-- DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;
DROP TEMPORARY TABLE IF EXISTS mediatemp;
CREATE TEMPORARY TABLE mediatemp (counted INT, flag INT, medianame VARCHAR(255));
SET count1 = 1;
SET count2 = 1;
SET countpages = (SELECT COUNT(*) FROM pjsons);
SET countmedia = (SELECT COUNT(*) FROM media WHERE package_id = packageid);
OPEN cur1;
OPEN cur2;
WHILE count2 <= countmedia DO
    FETCH cur2 into medianame;
        WHILE count1 <= countpages DO
            FETCH cur1 into a_json;
            SET jsonsearch = (SELECT JSON_SEARCH(a_json, 'one', medianame));
            IF jsonsearch IS NOT NULL THEN  SET existflag = 1; SET count1 = countpages;
            END IF;
            SET count1 = count1 + 1;
        END WHILE;
        CLOSE cur1;
        OPEN cur1;
        INSERT INTO mediatemp (flag, medianame, counted) VALUES (existflag, medianame, count2);
        SET existflag = 0;
        SET count2 = count2 + 1;
        SET count1 = 1;
END WHILE;
CLOSE cur1;
CLOSE cur2;
SELECT * FROM mediatemp WHERE flag = 0;
END
