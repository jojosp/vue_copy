CREATE DEFINER=`georgepan`@`%` PROCEDURE `clearassetsbeta`(IN packageid INT, IN modulesvarchar VARCHAR(255))
BEGIN
-- DECLARE a_json JSON;
-- DECLARE existflag INT DEFAULT 0;
-- DECLARE count1  INT;
-- DECLARE count2  INT;
-- DECLARE countpages  INT;
-- DECLARE countmedia  INT;
-- DECLARE jsonsearch VARCHAR(255);
-- DECLARE medianame VARCHAR(255);
-- DECLARE medianotused VARCHAR(255);
-- DECLARE allmedia VARCHAR(255);
-- DECLARE done BOOLEAN DEFAULT FALSE;
DROP TEMPORARY TABLE IF EXISTS pjsons;
SET @select2 = CONCAT('CREATE TEMPORARY TABLE IF NOT EXISTS pjsons as SELECT page_json FROM pages WHERE module_id IN (', modulesvarchar, ')');
-- SET @select2 = CONCAT('CREATE OR REPLACE VIEW pjsons as SELECT page_json FROM pages WHERE module_id IN (', modulesvarchar, ')');
PREPARE stm2 FROM @select2;
EXECUTE stm2;
DEALLOCATE PREPARE stm2;

-- SELECT filename FROM media WHERE package_id = packageid;
-- SELECT COUNT(*) FROM media WHERE package_id = packageid;
-- SELECT * FROM pjsons;
-- SELECT COUNT(*) FROM pjsons;
-- SELECT COUNT(*) FROM pages WHERE module_id IN (47, 48);
-- SELECT COUNT(*) FROM pjsons;
select m.filename as medianame
from media m
LEFT JOIN  pjsons p ON JSON_SEARCH(p.page_json,'one',m.filename) IS NOT NULL
WHERE m.package_id = packageid
GROUP BY m.filename
HAVING COUNT(p.page_json) = 0;
END
