import Vue from 'vue'
import { mount, shallow } from 'vue-test-utils'
import checktaptap from '@/ui-components/checktaptap/CheckTapTap.vue'
import eventbus from '@/config/eventhub.js'
describe('Render Tap Tap correctly', () => {
  let wrapper
  const el = JSON.parse("{\"key\":\"checktaptap\",\"name\":\"checktaptap\",\"ammount\":0,\"unlockammount\":1,\"showanswer\":0,\"bindchoices\":[],\"index\":0}")
  beforeEach(() => {
    wrapper = mount(checktaptap, {
      propsData: { el }
    })
  })
  it('render correct html', () => {
    expect(wrapper.element).toMatchSnapshot()
  })
  it('contains 4 tag a buttons RESET, CHECK, TRY AGAIN, SHOW', () => {
    expect(wrapper.findAll('a').length).toBe(4)
  })
  it('check click event functions', () => {
    let btnarray = wrapper.findAll('a')
    const spy = jest.fn()
    wrapper.setMethods({reset_answers: spy})
    wrapper.setMethods({check_answers: spy})
    wrapper.setMethods({tryagain: spy})
    wrapper.setMethods({show_answers: spy})
    wrapper.update()
    for (var i = 0; i < btnarray.length; i++) {
      btnarray.at(i).trigger('click')
    }
    expect(spy).toHaveBeenCalledTimes(btnarray.length)
  })
  it('check event taptap_clearinput', () => {
    // mock data
    let data = 0
    eventbus.fire('taptap_clearinput', data)
    if (data === 0) {
      expect(wrapper.vm.solved).toBe(0)
    }
  })
  it('check event taptap_clearinput', () => {
    // mock data
    let data = {
      evt: 'add',
      id: 1,
      componentid: 'tap'
    }
    eventbus.fire('checktaptap_hasinput', data)
    if (data.evt === 'add') {
      expect(wrapper.vm.useranswersarray.length).toBe(1)
    }
  })
  it('check event checktaptap_rightanswer', () => {
    let counter  = 1
    wrapper.vm.solved = counter
    wrapper.vm.ammount = 2
    eventbus.fire('checktaptap_rightanswer')
    expect(wrapper.vm.solved).toBe(counter + 1)
  })
})
