import Vue from 'vue'
import { mount, shallow } from 'vue-test-utils'
import taptap from '@/ui-components/taptap/TapTap.vue'
import VueTouch from 'vue-touch'
import eventbus from '@/config/eventhub.js'

describe('Render Tap Tap correctly', () => {
  let wrapper
  const el = JSON.parse("{\"id\":1,\"rightanswer\":\"\",\"tapinput\":\"1)......\",\"beforetext\":\"Text before tap input \",\"aftertext\":\" Text after tap input\",\"capitalize\":false,\"choicemultiple\":false,\"bindchoicesid\":\"tap\"}")
  beforeEach(() => {
    wrapper = mount(taptap, {
      propsData: { el }
    })
  })
  it('render correct html', () => {
    expect(wrapper.element).toMatchSnapshot()
  })
  it('contains v-touch', () => {
    expect(wrapper.contains('v-touch')).toBe(true)
  })
  it('check if taped input calls the inputclicked function', () => {
    let vtouchel = wrapper.find('v-touch')
    const spy = jest.fn()
    wrapper.setMethods({inputclicked: spy})
    wrapper.update()
    vtouchel.trigger('tap')
    expect(spy).toHaveBeenCalledTimes(1)
  })
  it('check if doubletaped input calls the resetchoices function', () => {
    let vtouchel = wrapper.find('v-touch')
    const spy = jest.fn()
    wrapper.setMethods({resetchoices: spy})
    wrapper.update()
    vtouchel.trigger('doubletap')
    expect(spy).toHaveBeenCalledTimes(1)
  })
  it('check event taptap_clearinput', () => {
    const spy = jest.fn()
    wrapper.vm.$on('taptap_clearinput', spy)
    wrapper.vm.$emit('taptap_clearinput', el.id)
    expect(spy).toBeCalledWith(el.id)
  })
  it('check event choicestaptap_userchoice', () => {
    let data = {
      id: 1
    }
    eventbus.fire('choicestaptap_userchoice', data)
    expect(wrapper.vm.userinputid).toBe(data.id)
  })
  it('check event taptap_disablereset', () => {
    let data = el.id
    eventbus.fire('taptap_disablereset', data)
    expect(wrapper.vm.disableinput).toBe(true)
  })
  it('check event choicestaptap_showrightanswers', () => {
    let data = {
      choiceid: el.bindchoicesid,
      array: [{id: 1, text: 'choice1'}]
    }
    eventbus.fire('choicestaptap_showrightanswers', data)
    expect(wrapper.vm.answers.length).toBe(0)
  })
})
