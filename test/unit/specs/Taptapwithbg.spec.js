import Vue from 'vue'
import { mount, shallow } from 'vue-test-utils'
import taptapwithbg from '@/sections/Body/Presentation/Taptapwithbg.vue'
import choicestaptap from '@/ui-components/choicestaptap/ChoicesTapTap.vue'
import taptap from '@/ui-components/taptap/TapTap.vue'
import VueTouch from 'vue-touch'
import eventbus from '@/config/eventhub.js'

describe('Render Choices Tap Tap correctly', () => {
  let wrapper
  const el = JSON.parse("{\"key\":\"taptapwithbg\",\"name\":\"taptap with background\",\"taps\":[{\"id\":1,\"rightanswer\":\"\",\"tapinput\":\"1)......\",\"beforetext\":\"Text before tap input \",\"aftertext\":\" Text after tap input\",\"capitalize\":false,\"choicemultiple\":false,\"bindchoicesid\":\"tap\"},{\"id\":2,\"rightanswer\":\"\",\"tapinput\":\"2)......\",\"beforetext\":\"Text before tap input \",\"aftertext\":\" Text after tap input\",\"capitalize\":false,\"choicemultiple\":false,\"bindchoicesid\":\"tap\"}],\"choices\":[{\"id\":1,\"text\":\"choice1\",\"selected\":false},{\"id\":2,\"text\":\"choice2\",\"selected\":false}],\"bgobject\":{},\"choiceid\":\"tap\",\"settingid\":0,\"area\":\"BODY\",\"index\":0}")
  beforeEach(() => {
    wrapper = mount(taptapwithbg, {
      propsData: { el },
      stubs: {'exp-taptap': taptap, 'exp-choicestaptap': choicestaptap}
    })
  })
  it('render correct html', () => {
    expect(wrapper.element).toMatchSnapshot()
  })
  it('fire event with choice data and check if component is clickable', () => {
    // find all tap tap components
    let taptaparray = wrapper.findAll(taptap)
    // for every tap tap send data to component through 'choicestaptap_userchoice' event
    for (var i = 0; i < taptaparray.length; i++) {
      let data = {
        text: el.choices[i].text,
        id: el.choices[i].id,
        choicestaptapid: el.choiceid
      }
      eventbus.fire('choicestaptap_userchoice', data)
      // check if component is clickable
      expect(taptaparray.at(i).vm.disableinput).toBe(false)
    }
  })
  it('fire event with choice data and run the function that puts the choice in one of the inputs', () => {
    Vue.nextTick(() =>{
      // find all tap tap components
      let taptaparray = wrapper.findAll(taptap)
      let vtoucharray = wrapper.findAll('v-touch')
      // for every tap tap send data to component through 'choicestaptap_userchoice' event
      for (var i = 0; i < taptaparray.length; i++) {
        let data = {
          text: el.choices[i].text,
          id: el.choices[i].id,
          choicestaptapid: el.choiceid
        }
        eventbus.fire('choicestaptap_userchoice', data)
        // load data to component variable through inputclicked function
        taptaparray.at(i).vm.inputclicked(el.choices[i].id)
        // check if hasinput component variable is true, indicates if tap tap component have a user choice
        expect(taptaparray.at(i).vm.hasinput).toBe(true)
      }
    })
  })
  it('fire event with choice data and run the function that puts the choice in one of the inputs. After that remove it with the function resetchoices', () => {
    Vue.nextTick(() =>{
      // find all tap tap components
      let taptaparray = wrapper.findAll(taptap)
      for (var i = 0; i < taptaparray.length; i++) {
        let data = {
          text: el.choices[i].text,
          id: el.choices[i].id,
          choicestaptapid: el.choiceid
        }
        // for every tap tap send data to component through 'choicestaptap_userchoice' events
        eventbus.fire('choicestaptap_userchoice', data)
        // load data to component variable through inputclicked function
        taptaparray.at(i).vm.inputclicked(el.choices[i].id)
        // remove user choice
        taptaparray.at(i).vm.resetchoices(el.choices[i].id)
        // check if hasinput component variable is true, indicates if tap tap component have a user choice
        expect(taptaparray.at(i).vm.hasinput).toBe(false)
      }
    })
  })
  it('fire event with choice data and run the function that puts the choice in one of the inputs. After that remove it with the function resetchoices and check if component is disabled', () => {
    Vue.nextTick(() =>{
      // find all tap tap components
      let taptaparray = wrapper.findAll(taptap)
      let choiceel = wrapper.find(choicestaptap)
      for (var i = 0; i < taptaparray.length; i++) {
        let data = {
          text: el.choices[i].text,
          id: el.choices[i].id,
          choicestaptapid: el.choiceid
        }
        // for every tap tap send data to component through 'choicestaptap_userchoice' events
        eventbus.fire('choicestaptap_userchoice', data)
        // load data to component variable through inputclicked function
        taptaparray.at(i).vm.inputclicked(el.choices[i].id)
        // remove user choice
        taptaparray.at(i).vm.resetchoices(el.choices[i].id)
        // check if component is disabled after user double tap his/her choice
        expect(taptaparray.at(i).vm.disableinput).toBe(true)
      }
    })
  })
  it('fire event taptap_clearinput, demonstrate reset button', () => {
    Vue.nextTick(() =>{
      // find all tap tap components
      let taptaparray = wrapper.findAll(taptap)
      for (var i = 0; i < taptaparray.length; i++) {
        let data = {
          text: el.choices[i].text,
          id: el.choices[i].id,
          choicestaptapid: el.choiceid
        }
        // for every tap tap send data to component through 'choicestaptap_userchoice' events
        eventbus.fire('choicestaptap_userchoice', data)
        // load data to component variable through inputclicked function
        taptaparray.at(i).vm.inputclicked(el.choices[i].id)
      }
      // reset event
      eventbus.fire('taptap_clearinput', 0)


      // check if components have inputs
      for (var j = 0; j < taptaparray.length; j++) {
        expect(taptaparray.at(j).vm.hasinput).toBe(false)
      }
    })
  })
})
